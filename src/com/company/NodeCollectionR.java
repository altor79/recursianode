package com.company;

import javafx.scene.Node;

public class NodeCollectionR implements NodeRecurs {
    private NodeR node;

    @Override
    public void add(int x) {
        if (node == null) {
            node = new NodeR(x);
        } else {
            NodeR node = this.node;
            while (node.next != null) {
                node = node.next;
            }
            NodeR newNode = new NodeR(x);
            node.next = newNode;
        }
    }

    @Override
    public void addSet(int x, int index) {

        NodeR nodeTmp = node;
        for (int i = 0; i < index - 1; i++) {
            nodeTmp = nodeTmp.next;
        }
        NodeR node = new NodeR(x);
        if (nodeTmp.next != null) {
            node.next = nodeTmp.next;
        }
        nodeTmp.next = node;
    }


    @Override
    public void set(int x, int index) {
        int s = 0;
        NodeR node = this.node;
        while (index > ++s) {
            node = node.next;
        }
        node.value = x;
    }

    @Override
    public Integer get(int index) {
        int s = 0;
        if (node == null) {
            return null;
        } else {
            NodeR node = this.node;
            while (index > s++) {
                node = node.next;
            }
            return node.value;
        }
    }

    @Override
    public int size() {
        if (node == null) {
            return 0;
        } else {
            int index = 0;
            NodeR node = this.node;
            while (node != null) {
                index++;
                node = node.next;
            }
            return index;
        }
    }

    @Override
    public int sizeR() {

        return calculateSize(node, 0);
    }

    private int calculateSize(NodeR node, int current) {
        if (node == null) return current;
        return calculateSize(node.next, ++current);
    }

    @Override
    public int nodeMaxi() {
        if (node == null) return -1;
        return nodeMax(node.next, 0, node.value, 0);
    }

    private int nodeMax(NodeR node, int index, int value, int maxIndex) {
        if (node == null) return maxIndex;
        index++;
        if (node.value > value) {
            return nodeMax(node.next, index, node.value, index);
        }
        return nodeMax(node.next, index, value, maxIndex);
    }
    @Override
    public int nodeMaxiValue() {
        if (node == null) return -1;
        return nodeMaxValue(node, 0, node.value);
    }
    private int nodeMaxValue(NodeR node, int index, int valueV) {
        if (node == null) return valueV;
        index++;

        if (node.value > valueV) {
            return nodeMaxValue(node.next, index, node.value);
        }
        return nodeMaxValue(node.next, index, valueV);
    }




    @Override
    public void clear() {
        node = null;
    }

}
